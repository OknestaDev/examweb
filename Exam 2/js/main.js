$(document).ready(function() {
    $('.team_cards--slider').bxSlider({
        controls: false,
        keyboardEnabled: true
    });

    $('.testimonials_cards--slider').bxSlider({
        controls: false,
        keyboardEnabled: true,
        auto: true
    });

    smoothScroll.init({
        selector: '[data-scroll]',
        selectorHeader: null,
        speed: 500,
        easing: 'ease',
        offset: 0
    });

    let grid = $('.portfolio_works').isotope({
        itemSelector: '.portfolio_worksItem',
        layoutMode: 'masonry',
        masonry: {
            horizontalOrder: true
        }
    });

    $('.portfolio_filter').click(function() {
        let filterValue = $( this ).attr('data-filter');
        grid.isotope({ filter: filterValue });

        $(".portfolio_filter").removeClass("portfolio_activfilter");
        $(this).addClass("portfolio_activfilter");
    });

    $('.top-menu__item').click(function() {
        $(".top-menu__item").removeClass("active-menu-link");
        $(this).addClass("active-menu-link");
    });
});

(function () {
    window.onload = function () {
        let map,
            point = {lat: 47.8159221, lng: 35.1685268},
            iv1Content = document.querySelector('.info-window-1');

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: point,
                zoom: 17
            });
            let marker = new google.maps.Marker({
                position: point,
                map: map,
                title:"Academy",
                icon: "favicon.png"
            });
        }
        initMap();
    }
})();


